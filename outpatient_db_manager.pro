QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/patientinfo.cpp \
    src\database.cpp \
    src\images.cpp \
    src\main.cpp \
    src\mainwindow.cpp \
    src\save_patient_info.cpp \
    src\view_patient_info.cpp

HEADERS += \
    src/patientinfo.h \
    src\database.h \
    src\images.h \
    src\mainwindow.h \
    src\save_patient_info.h \
    src\view_patient_info.h

FORMS += \
    src\images.ui \
    src\mainwindow.ui \
    src\save_patient_info.ui \
    src\view_patient_info.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
