#ifndef SAVE_PATIENT_INFO_H
#define SAVE_PATIENT_INFO_H

#include <QDialog>

#include "database.h"

namespace Ui {
class PatientInfo;
}

class SavePatientInfo : public QDialog
{
    Q_OBJECT

public:
    explicit SavePatientInfo( QWidget* parent = nullptr, const QString& dbPath = QString() );
    ~SavePatientInfo();

    bool setDbPath( const QString& dbPath );

private slots:

    void onSavePatientButtonClicked();

    void onCancelButtonClicked();

private:
    Ui::PatientInfo* ui;

    Database m_db;

    QString m_dbPath;
};

#endif // SAVE_PATIENT_INFO_H
