#ifndef IMAGES_H
#define IMAGES_H

#include <QDialog>
#include <QtSql>

#include "database.h"

namespace Ui {
class Images;
}

class Images : public QDialog
{
    Q_OBJECT

public:
    explicit Images( QWidget* parent = nullptr, const QString& dbPath = QString() );
    ~Images();

    void redrawImageTable();

    const QString& getPatientIndex() const;

    void setPatientIndex( const QString& index );

    bool setDbPath( const QString& dbPath );

    void clearImageLabel();

private slots:
    void onLoadImageButtonclicked();

    void onTableViewActivated( const QModelIndex& index );

    void onDeleteImageButtonclicked();

private:
    Ui::Images* ui;

    Database m_db;

    QSqlTableModel* m_pImagesModel;

    QString m_patientIndex;

    QString m_dbPath;

};

inline const QString& Images::getPatientIndex() const
{
    return m_patientIndex;
}

#endif // IMAGES_H
