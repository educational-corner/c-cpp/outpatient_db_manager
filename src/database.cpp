#include "database.h"

#include <QMessageBox>

Database::Database()
{
    m_db = QSqlDatabase::addDatabase( "QSQLITE" );
}

Database::Database( const QString& dbPath )
    :m_dbPath( dbPath )
{
    m_db = QSqlDatabase::addDatabase( "QSQLITE" );
    if( !m_db.isOpen() )
        connectToDb();
}

Database::~Database()
{
    if ( m_db.isOpen() )
        quitDb();
}

bool Database::connectToDb()
{
    if( m_db.isOpen() )
    {
        quitDb();
    }

    m_db.setDatabaseName( m_dbPath );

    if( m_db.open() )
    {
        qDebug() << "Connected!";
        if ( !m_db.tables().contains( QLatin1String( "patients" ) ) )
            createPatientsTable();
        if ( !m_db.tables().contains( QLatin1String( "images" ) ) )
            createImagesTable();

        return true;
    }
    else
    {
        qDebug() << "Not connected!";
        return false;
    }
}
void Database::quitDb()
{
    m_db.close();
    qDebug() << "Disconnected!";
}

bool Database::addNewPatient( const QString & fullName,
                              const QString & birthDate,
                              const QString & address,
                              const QString & appDate,
                              const QString & dischDate )
{
    if( !m_db.isOpen() )
    {
        connectToDb();
    }
    QSqlQuery query( m_db );

    query.prepare( "INSERT INTO patients"
                               " ( full_name, birth_year, adress, admission_date, discharge_date ) "
                               "VALUES ( :fullName, :birthDate, :address, :appDate, :dischDate )" );

    query.bindValue( ":fullName", fullName );
    query.bindValue( ":birthDate", birthDate );
    query.bindValue( ":address", address );
    query.bindValue( ":appDate", appDate );
    query.bindValue( ":dischDate", dischDate );

    if( query.exec() )
    {
        return true;
    }
    else
    {
        qDebug() << query.lastError().text();
        return false;
    }
}

void Database::updatePatientInfo( const QString& patientID,
                                  const QString& fullName,
                                  const QString& birthDate,
                                  const QString& address,
                                  const QString& appDate,
                                  const QString& dischDate )
{
    if( !m_db.isOpen() )
    {
        connectToDb();
    }
    QSqlQuery query( m_db );

    query.prepare( "UPDATE patients"
                       " SET full_name = :fullName,"
                       " birth_year = :birthDate,"
                       " adress = :address,"
                       " admission_date = :appDate,"
                       " discharge_date = :dischDate"
                       " WHERE id = :patientID" );

    query.bindValue( ":fullName", fullName );
    query.bindValue( ":birthDate", birthDate );
    query.bindValue( ":address", address );
    query.bindValue( ":appDate", appDate );
    query.bindValue( ":dischDate", dischDate );
    query.bindValue( ":patientID", patientID );

    if( !query.exec() )
    {

        qDebug() << query.lastError().text();
    }

}

bool Database::addNewImageToPatient( const QByteArray& pic,
                           const QString& dateImage,
                           const QString& patientId )
{
    if( !m_db.isOpen() )
    {
        connectToDb();
    }
    QSqlQuery query( m_db );

    query.prepare( "INSERT INTO images"
                                  " ( image_file, date, patient_id ) "
                                  "VALUES ( :pics, :dateImg, :patID )" );
    query.bindValue( ":pics", pic );
    query.bindValue( ":dateImg", dateImage );
    query.bindValue( ":patID", patientId );

    if( query.exec() )
    {
        return true;
    }
    else
    {
        qDebug() << query.lastError().text();
        return false;
    }
}

bool Database::deletePatientByID( const QString& patientID )
{
    if( !m_db.isOpen() )
    {
        connectToDb();
    }
    QSqlQuery query( m_db );

    setForeignKeysTrue();

    query.prepare( "DELETE FROM patients WHERE id = :patID" );
    query.bindValue( ":patID", patientID );

    if( query.exec() )
    {
        return true;
    }
    else
    {
        qDebug() << query.lastError().text();
        return false;
    }
}

bool Database::deleteImagesByPatientID( const QString& patientID )
{
    if( !m_db.isOpen() )
    {
        connectToDb();
    }
    QSqlQuery query( m_db );

    query.prepare( "DELETE FROM images WHERE patient_id = :patID" );
    query.bindValue( ":patID", patientID );

    if( query.exec() )
    {
        return true;
    }
    else
    {
        qDebug() << query.lastError().text();
        return false;
    }
}

bool Database::deleteImageByID( const QString & imageID )
{
    if( !m_db.isOpen() )
    {
        connectToDb();
    }
    QSqlQuery query( m_db );

    query.prepare( "DELETE FROM images WHERE id = :imgID" );
    query.bindValue( ":imgID", imageID );

    if( query.exec() )
    {
        return true;
    }
    else
    {
        qDebug() << query.lastError().text();
        return false;
    }
}

void Database::setForeignKeysTrue()
{
    if( !m_db.isOpen() )
    {
        connectToDb();
    }
    QSqlQuery query( m_db );

    query.prepare( "PRAGMA foreign_keys = true" );

    if( !query.exec() )
    {
        qDebug() << query.lastError().text();
    }
}

PatientInfo* Database::getPatientInfo( const QString& patientID )
{
    if( !m_db.isOpen() )
    {
        connectToDb();
    }
    QSqlQuery query( m_db );

    PatientInfo* data = nullptr;

    query.prepare( "SELECT * FROM patients WHERE id = :patID" );
    query.bindValue( ":patID", patientID );

    if( query.exec() )
    {
        data = new PatientInfo();

        while( query.next() )
        {
            data->setID( query.value( 0 ).toString() );
            data->setName( query.value( 1 ).toString() );
            data->setBirthDate( query.value( 2 ).toString() );
            data->setCity( query.value( 3 ).toString() );
            data->setAppDate( query.value( 4 ).toString() );
            data->setDischDate( query.value( 5 ).toString() );
        }
    }

    return data;
}

void Database::setDbPath( const QString& dbPath )
{
    if( dbPath.isEmpty() )
        return;

    if( m_db.isOpen() )
        quitDb();

    m_dbPath = dbPath;

    connectToDb();
}

bool Database::isOpenConnection() const
{
    return m_db.isOpen();
}

bool Database::createPatientsTable()
{
    QSqlQuery query( m_db );

    query.prepare( "CREATE TABLE patients ( id integer primary key,"
                                           "full_name text not null,"
                                           "birth_year integer not null,"
                                           "adress text not null,"
                                           "admission_date text not null,"
                                           "discharge_date text not null );" );

    if( !query.exec() )
    {
        qDebug() << query.lastError().text();
        return false;
    }
    return true;
}

bool Database::createImagesTable()
{
    QSqlQuery query( m_db );

    query.prepare( "CREATE TABLE images (id integer primary key,"
                                       " image_file BLOB not null,"
                                       " date text not null,"
                                       " patient_id integer not null,"
                                       " foreign key (patient_id) references patients(id) on delete cascade);" );

    if( !query.exec() )
    {
        qDebug() << query.lastError().text();
        return false;
    }
    return true;
}
