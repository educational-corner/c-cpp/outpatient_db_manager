#include "mainwindow.h"

#include <QFile>
#include <QFileDialog>
#include <QMessageBox>

#include "images.h"
#include "save_patient_info.h"
#include "ui_mainwindow.h"
#include "view_patient_info.h"




MainWindow::MainWindow( QWidget *parent )
    : QMainWindow( parent )
    , ui( new Ui::MainWindow )
{
    ui->setupUi( this );

    ui->tableView->setSelectionBehavior( QAbstractItemView::SelectRows );
    ui->tableView->setSelectionMode( QAbstractItemView::SingleSelection );
    ui->tableView->resizeColumnsToContents();
    ui->tableView->setEditTriggers( QAbstractItemView::NoEditTriggers );
    ui->tableView->horizontalHeader()->setStretchLastSection( true );

    connect( ui->RedrawTableButton, &QPushButton::clicked, this, &MainWindow::onRedrawTableButtonClicked );
    connect( ui->addPatientButton, &QPushButton::clicked, this, &MainWindow::onAddPatientButtonClicked );
    connect( ui->connectToDBButton, &QPushButton::clicked, this, &MainWindow::onConnectToDBButtonClicked );
    connect( ui->tableView, &QTableView::activated, this, &MainWindow::onTableViewActivated );
}

MainWindow::~MainWindow()
{
    delete ui;

    delete m_pSavePatientInfo;
    delete m_pViewPatientInfo;

    delete m_pPatientsTableModel;
}

void MainWindow::onAddPatientButtonClicked()
{
    if( m_dbPath.isEmpty() )
    {
        QMessageBox::critical( this, tr( "error::" ), tr( "Not connected to database, try \"Connect to data base\" button" ) );
        return;
    }

    if( m_pSavePatientInfo->exec() )
    {
        redrawTable();
    }
}

void MainWindow::redrawTable()
{
    m_pPatientsTableModel->select();
}


void MainWindow::onTableViewActivated( const QModelIndex &index )
{
    QString patientIndex = ui->tableView->model()->data( ui->tableView->model()->index( index.row(), 0 ) ).toString();
    m_pViewPatientInfo->setPatientIndex( patientIndex );

    m_pViewPatientInfo->showInfo();

    if( m_pViewPatientInfo->exec() )
    {
        redrawTable();
    }

}


void MainWindow::onRedrawTableButtonClicked()
{
    if( m_dbPath.isEmpty() )
    {
        QMessageBox::critical( this, tr( "error::" ), tr( "Not connected to database, try \"Connect to data base\" button" ) );
        return;
    }
    redrawTable();
}


void MainWindow::onConnectToDBButtonClicked()
{
    QString dbPath = QFileDialog::getSaveFileName( this,
                                                   tr( "Open File" ),
                                                   "/home",
                                                   tr( "Files ( *.db )" ) );

    if( m_dbPath == dbPath || dbPath.isEmpty() )
        return;

    m_dbPath = dbPath;
    qDebug() << m_dbPath;

    if( !QFile::exists( m_dbPath ) )
    {
        QFile dbFile( m_dbPath );

        if( !dbFile.open( QFile::ReadWrite ) )
        {
            qDebug() << "can't open file!";
        }
        dbFile.close();
    }

    if( m_pSavePatientInfo == nullptr )
    {
        m_pSavePatientInfo = new SavePatientInfo( nullptr, m_dbPath );
        m_pViewPatientInfo = new ViewPatientInfo( nullptr, m_dbPath );
    }
    else
    {
        m_pSavePatientInfo->setDbPath( m_dbPath );
        m_pViewPatientInfo->setDbPath( m_dbPath );
    }

    m_db.setDbPath( m_dbPath );


    if( m_pPatientsTableModel )
        delete m_pPatientsTableModel;

    m_pPatientsTableModel = new QSqlTableModel( this, m_db.getDB() );

    m_pPatientsTableModel->setTable( "patients" );

    ui->tableView->setModel( m_pPatientsTableModel );

    redrawTable();
}

