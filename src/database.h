#ifndef DATABASE_H
#define DATABASE_H

#include <QtSql>

#include "patientinfo.h"

class Database
{
public:

    Database();
    Database( const QString& dbPath );
    ~Database();

    bool isOpenConnection() const;

    bool addNewPatient(const QString& fullName,
                       const QString& address,
                       const QString& birthDate,
                       const QString& appDate,
                       const QString& dischDate);

    bool addNewImageToPatient( const QByteArray& pic,
                               const QString& dateImage,
                               const QString& patientId );

    bool deletePatientByID( const QString& patientID );

    bool deleteImagesByPatientID( const QString& patientID );

    bool deleteImageByID( const QString& imageID );

    void updatePatientInfo( const QString& patientID,
                            const QString& fullName,
                            const QString& birthDate,
                            const QString& address,
                            const QString& appDate,
                            const QString& dischDate );

    PatientInfo* getPatientInfo( const QString& patientID );

    void setForeignKeysTrue();

    void setDbPath( const QString& dbPath );

private:

    bool createPatientsTable();
    bool createImagesTable();

public:

    bool connectToDb();
    void quitDb();

private:

    QSqlDatabase m_db;
    QString m_dbPath;

public:

    QSqlDatabase& getDB();
};

inline QSqlDatabase& Database::getDB()
{
    return m_db;
}

#endif // DATABASE_H
