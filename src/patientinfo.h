#ifndef PATIENTINFO_H
#define PATIENTINFO_H

#include <QDate>
#include <QString>

class PatientInfo
{
public:
    PatientInfo();

    PatientInfo( const QString& _id,
                 const QString& _name,
                 const QString& _birthDate,
                 const QString& _city,
                 const QString& _appDate,
                 const QString& _dischDate );

    const QString& getID() const;
    const QString& getName() const;
    const QString& getStrBirthDate() const;
    const QString& getCity() const;
    const QString& getStrAppDate() const;
    const QString& getStrDischDate() const;

    QDate* getDate( const QString& _strDate) const;

    void setID( const QString& _id );
    void setName( const QString& _name );
    void setBirthDate( const QString& _birthDate );
    void setCity( const QString& _city );
    void setAppDate( const QString& _appDate );
    void setDischDate( const QString& _dischDate );

private:

    void splitDateStringIntoInt( const QString& strDate, int& day, int& month, int& year ) const;

private:

    QString m_id;
    QString m_name;
    QString m_birthDate;
    QString m_city;
    QString m_appDate;
    QString m_dischDate;
};

inline const QString& PatientInfo::getID() const
{
    return m_id;
};
inline const QString& PatientInfo::getName() const
{
    return m_name;
};
inline const QString& PatientInfo::getStrBirthDate() const
{
    return m_birthDate;
};
inline const QString& PatientInfo::getCity() const
{
    return m_city;
};
inline const QString& PatientInfo::getStrAppDate() const
{
    return m_appDate;
};
inline const QString& PatientInfo::getStrDischDate() const
{
    return m_dischDate;
};

#endif // PATIENTINFO_H
