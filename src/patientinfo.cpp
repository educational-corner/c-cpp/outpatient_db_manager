#include "patientinfo.h"

PatientInfo::PatientInfo()
{}

PatientInfo::PatientInfo( const QString& _id,
                          const QString& _name,
                          const QString& _birthDate,
                          const QString& _city,
                          const QString& _appDate,
                          const QString& _dischDate )
    :m_id( _id ),
     m_name( _name ),
     m_birthDate( _birthDate ),
     m_city( _city ),
     m_appDate( _appDate ),
     m_dischDate( _dischDate )
{}

void PatientInfo::setID( const QString& _id )
{
    m_id = _id;
};
void PatientInfo::setName( const QString& _name )
{
    m_name = _name;
};
void PatientInfo::setBirthDate( const QString& _birthDate )
{
    m_birthDate = _birthDate;
};
void PatientInfo::setCity( const QString& _city )
{
    m_city = _city;
};
void PatientInfo::setAppDate( const QString& _appDate )
{
    m_appDate = _appDate;
};
void PatientInfo::setDischDate( const QString& _dischDate )
{
    m_dischDate = _dischDate;
};

QDate* PatientInfo::getDate( const QString& _strDate ) const
{
    int day, month, year;
    splitDateStringIntoInt( _strDate, day, month, year );
    return new QDate( year, month, day );
}

void PatientInfo::splitDateStringIntoInt( const QString& strDate, int& day, int& month, int& year ) const
{
    const char* ddMMYYYY = strDate.toStdString().c_str();

    char sep1, sep2;
    int nMatched = sscanf( ddMMYYYY, "%d%c%d%c%d",
                           & day, & sep1, & month, & sep2, & year );

    if ( nMatched != 5 || sep1 != '.' || sep2 != '.' )
        qDebug() << "date format is incorrect!";
}
