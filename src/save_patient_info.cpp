#include "save_patient_info.h"

#include <QMessageBox>

#include "ui_save_patient_info.h"

SavePatientInfo::SavePatientInfo( QWidget *parent, const QString& dbPath ) :
    QDialog( parent ),
    ui( new Ui::PatientInfo ),
    m_dbPath( dbPath )
{
    ui->setupUi( this );

    ui->admissionDateEdit->setDate( QDate::currentDate() );
    ui->dischargeDateEdit->setDate( QDate::currentDate() );

    ui->admissionDateEdit->setMaximumDate( QDate::currentDate() );
    ui->dischargeDateEdit->setMinimumDate( QDate::currentDate() );

    connect( ui->savePatientButton, &QPushButton::clicked, this, &SavePatientInfo::onSavePatientButtonClicked );
    connect( ui->cancelButton, &QPushButton::clicked, this, &SavePatientInfo::onCancelButtonClicked );

    m_db.setDbPath( dbPath );
}

SavePatientInfo::~SavePatientInfo()
{
    delete ui;
}

void SavePatientInfo::onSavePatientButtonClicked()
{

    QString admissDate = ui->admissionDateEdit->text();

    QString dischDate = ui->dischargeDateEdit->text();

    QString birthYear = ui->birthDate->text();

    QString name = ui->str_name_edit->text();

    QString address = ui->str_address_edit->text();

    if( name.isEmpty() || address.isEmpty() )
    {
        QMessageBox::information( this, tr( "error: " ), tr( "Name & Adress can't be empty" ) );
        return;
    }
    else
    {
        if( m_db.addNewPatient( name, birthYear, address, admissDate, dischDate ) )
        {
            this->accept();
        }
    }
}


void SavePatientInfo::onCancelButtonClicked()
{
    this->reject();
}

bool SavePatientInfo::setDbPath( const QString& dbPath )
{
    if( dbPath.isEmpty() )
        return false;

    m_dbPath = dbPath;
    m_db.setDbPath( m_dbPath );
    return true;
}

