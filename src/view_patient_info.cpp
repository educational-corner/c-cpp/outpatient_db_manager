#include "view_patient_info.h"

#include <QMessageBox>
#include <QVariantList>


#include "images.h"
#include "mainwindow.h"
#include "ui_view_patient_info.h"

ViewPatientInfo::ViewPatientInfo( QWidget *parent, const QString& dbPath ) :
    QDialog( parent ),
    m_dbPath( dbPath ),
    ui( new Ui::ViewPatientInfo )
{
    m_pImages = new Images( nullptr, m_dbPath );

    ui->setupUi( this );

    m_db.setDbPath( m_dbPath );



    connect( ui->UpdatePatientInfoButton, &QPushButton::clicked, this, &ViewPatientInfo::onUpdatePatientInfoButtonClicked);
    connect( ui->ViewPhotosButton, &QPushButton::clicked, this, &ViewPatientInfo::onViewPhotosButtonClicked);
    connect( ui->DeletePatientButton, &QPushButton::clicked, this, &ViewPatientInfo::onDeletePatientButtonClicked);
}

ViewPatientInfo::~ViewPatientInfo()
{
    if( m_pImages )
        delete m_pImages;

    delete ui;
}

void ViewPatientInfo::showInfo()
{

    m_pPatientInfo = m_db.getPatientInfo( getPatientIndex() );

    if( m_pPatientInfo )
    {
        ui->PatientIDlabel->setText( m_pPatientInfo->getID() );

        ui->PatientNameEdit->setText( m_pPatientInfo->getName() );

        QDate* pBirthDate = m_pPatientInfo->getDate( m_pPatientInfo->getStrBirthDate() );
        ui->BirthdateEdit->setDate( *pBirthDate );

        ui->PatientCityEdit->setText( m_pPatientInfo->getCity() );

        QDate* pAppDate = m_pPatientInfo->getDate( m_pPatientInfo->getStrAppDate() );
        ui->AdmissionDateEdit->setDate( *pAppDate );

        QDate* pDischDate = m_pPatientInfo->getDate( m_pPatientInfo->getStrDischDate() );
        ui->DischargeDateEdit->setDate( *pDischDate );

        delete pDischDate;
        delete pAppDate;
        delete pBirthDate;
        delete m_pPatientInfo;
    }
}

void ViewPatientInfo::onDeletePatientButtonClicked()
{
    QString id = getPatientIndex();

    QMessageBox::StandardButton deleteOrNot;
    deleteOrNot = QMessageBox::question( this,
                                         "Delete An Image", "Are you sure you want to delete"
                                                            " patient with current id '"+id+"' and all his images?",
                                         QMessageBox::Yes|QMessageBox::No );

    if( deleteOrNot == QMessageBox::Yes )
    {

        if( m_db.deletePatientByID( id ) )
            this->accept();
    }
}


void ViewPatientInfo::onViewPhotosButtonClicked()
{
    m_pImages->setDbPath( m_dbPath );

    m_pImages->setPatientIndex( m_patientIndex );

    m_pImages->redrawImageTable();

    if( m_pImages->exec() )
        m_pImages->clearImageLabel();
}

void ViewPatientInfo::setPatientIndex( const QString& index )
{
    m_patientIndex = index;
}


void ViewPatientInfo::onUpdatePatientInfoButtonClicked()
{
    QString name = ui->PatientNameEdit->text();

    QString address = ui->PatientCityEdit->text();

    if( name.isEmpty() || address.isEmpty() )
    {
        QMessageBox::information( this, tr( "error: " ), tr( "Name & Adress can't be empty" ) );
        return;
    }
    else
    {

        m_db.updatePatientInfo( getPatientIndex(),
                                  ui->PatientNameEdit->text(),
                                  ui->BirthdateEdit->text(),
                                  ui->PatientCityEdit->text(),
                                  ui->AdmissionDateEdit->text(),
                                  ui->DischargeDateEdit->text() );

        this->accept();
    }
}

bool ViewPatientInfo::setDbPath( const QString& dbPath )
{
    if( dbPath.isEmpty() )
        return false;

    m_dbPath = dbPath;
    m_db.setDbPath( m_dbPath );
    m_pImages->setDbPath( dbPath );
    return true;
}
