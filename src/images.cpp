#include "images.h"

#include <QFileDialog>
#include <QMessageBox>

#include "ui_images.h"


Images::Images( QWidget* parent, const QString& dbPath ) :
    QDialog( parent ),
    m_dbPath( dbPath ),
    ui( new Ui::Images )
{
    ui->setupUi( this );

    m_db.setDbPath( dbPath );

    m_pImagesModel = new QSqlTableModel( this, m_db.getDB() );
    m_pImagesModel->setTable( "images" );

    ui->tableView->setModel( m_pImagesModel );



    ui->tableView->setSelectionBehavior( QAbstractItemView::SelectRows );
    ui->tableView->setSelectionMode( QAbstractItemView::SingleSelection );
    ui->tableView->resizeColumnsToContents();
    ui->tableView->setEditTriggers( QAbstractItemView::NoEditTriggers );
    ui->tableView->horizontalHeader()->setStretchLastSection( true );

    connect( ui->LoadImageButton, &QPushButton::clicked, this, &Images::onLoadImageButtonclicked );
    connect( ui->DeleteImageButton, &QPushButton::clicked, this, &Images::onDeleteImageButtonclicked );
    connect( ui->tableView, &QTableView::activated, this, &Images::onTableViewActivated );
}

Images::~Images()
{

    delete m_pImagesModel;

    delete ui;
}

void Images::onLoadImageButtonclicked()
{
    QString fileName = QFileDialog::getOpenFileName( this, tr( "Open File" ),
                                                      "/home",
                                                      tr( "Images ( *.jpg )" ) );
    if( fileName.isEmpty() )
        return;

    QPixmap pix( fileName );
    QByteArray inByteArray;
    QBuffer inBuffer( &inByteArray );
    inBuffer.open( QIODevice::WriteOnly );
    pix.save( &inBuffer, "JPG" );

    QFileInfo imageInfo;
    imageInfo.setFile(fileName);

    QDate fileCreationDate = QFileInfo(fileName).lastModified().date();

    if( m_db.addNewImageToPatient( inByteArray, fileCreationDate.toString(), m_patientIndex ) )
    {
        redrawImageTable();
    }
}

void Images::redrawImageTable()
{
    m_pImagesModel->select();
    ui->tableView->setColumnHidden( 1, true );
    ui->tableView->setColumnHidden( 3, true );
}


void Images::onTableViewActivated( const QModelIndex &index )
{
    QPixmap outPixmap = QPixmap();
    outPixmap.loadFromData( m_pImagesModel->record(index.row()).value( "image_file" ).toByteArray());

    ui->ImageLabel->setPixmap( outPixmap );
}

void Images::onDeleteImageButtonclicked()
{
    QModelIndex currentDiscount  = ui->tableView->currentIndex();
    if( currentDiscount.isValid() )
    {
        QString imageID = ui->tableView->model()->data( ui->tableView->model()->index( currentDiscount.row(),0 ),0 ).toString();
        QMessageBox::StandardButton deleteOrNot;
        deleteOrNot = QMessageBox::question( this,
                                             "Delete An Image", "Are you sure you want to delete"
                                                                " an image with current index '"+imageID+"'?",
                                             QMessageBox::Yes|QMessageBox::No );

        if( deleteOrNot == QMessageBox::Yes )
        {
            if ( m_db.deleteImageByID( imageID ) )
            {
                redrawImageTable();
                ui->ImageLabel->clear();
            }
        }
    }
    else
    {
        QMessageBox::information( this, tr( "Info" ), tr ( "Select an image from the table to delete!" ) );
    }
}

void Images::setPatientIndex( const QString& index )
{
    m_patientIndex = index;

    m_pImagesModel->setTable( "images" );
    m_pImagesModel->setFilter( "patient_id='"+m_patientIndex+"'" );
}

bool Images::setDbPath( const QString& dbPath )
{
    if( dbPath.isEmpty() )
        return false;

    m_dbPath = dbPath;
    m_db.setDbPath( m_dbPath );

    if( m_pImagesModel )
    {
        delete m_pImagesModel;

        m_pImagesModel = new QSqlTableModel( this, m_db.getDB() );
        m_pImagesModel->setTable( "images" );

        ui->tableView->setModel( m_pImagesModel );
    }

    return true;
}

void Images::clearImageLabel()
{
    ui->ImageLabel->clear();
}

