#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSql>

#include "database.h"

class SavePatientInfo;
class ViewPatientInfo;
class Images;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow( QWidget* parent = nullptr );
    ~MainWindow();

    void redrawTable();

private slots:

    void onAddPatientButtonClicked();

    void onTableViewActivated( const QModelIndex &index );

    void onRedrawTableButtonClicked();

    void onConnectToDBButtonClicked();

private:
    Ui::MainWindow* ui = nullptr;

    QSqlTableModel* m_pPatientsTableModel = nullptr;

private:

    Database m_db;

    SavePatientInfo* m_pSavePatientInfo = nullptr;

    ViewPatientInfo* m_pViewPatientInfo = nullptr;

    QString m_dbPath;

};
#endif // MAINWINDOW_H
