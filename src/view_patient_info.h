#ifndef VIEW_PATIENT_INFO_H
#define VIEW_PATIENT_INFO_H

#include <QDialog>

#include "database.h"
#include "images.h"
#include "patientinfo.h"


namespace Ui {
class ViewPatientInfo;
}

class ViewPatientInfo : public QDialog
{
    Q_OBJECT

public:

    explicit ViewPatientInfo( QWidget* parent = nullptr, const QString& dbPath = QString() );
    ~ViewPatientInfo();

    void showInfo();

    const QString& getPatientIndex() const;
    void setPatientIndex( const QString& index );

    bool setDbPath( const QString& dbPath );

private slots:

    void onDeletePatientButtonClicked();

    void onViewPhotosButtonClicked();

    void onUpdatePatientInfoButtonClicked();

private:
    Ui::ViewPatientInfo *ui;

    Database m_db;

    Images* m_pImages = nullptr;

    QString m_patientIndex;

    QString m_dbPath;

    PatientInfo* m_pPatientInfo = nullptr;

};

inline const QString& ViewPatientInfo::getPatientIndex() const
{
    return m_patientIndex;
}

#endif // VIEW_PATIENT_INFO_H
